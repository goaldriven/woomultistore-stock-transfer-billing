<?php
/**
 * Plugin Name:     Woomultistore Stock Transfer Billing
 * Plugin URI:      https://bitbucket.org/goaldriven/woomultistore-stock-transfer-billing
 * Description:     (for Multisite only) (for Subsite only) add stock transfer feature to the 'Woomultistore Automated Subsite Billing'
 * Author:          GoalDriven
 * Author URI:      https://goaldriven.co?s=Woomultistore+Stock+Transfer+Billing
 * Text Domain:     woomultistore-stock-transfer-billing
 * Domain Path:     /languages
 * Version:         1.0.5
 *
 * @package         Woomultistore_Stock_Transfer_Billing
 */

try {
	require_once( __DIR__ . '/vendor/autoload.php' );
} catch ( Exception $e ) {
	return;
}

define( 'WSTB_DIR', __DIR__ );

add_action( 'setup_theme', function () {
	if ( ! class_exists( 'WooCommerce_Role_Based_Price_Product_Pricing' ) ) {
		add_action( 'admin_notices', function () {
			?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e( 'WSTB: Please install and activate \'WooCommerce Role Based Price\'' ); ?></p>
            </div>
			<?php
		} );

		return;
	}

	if ( ! defined( 'WASB_DIR' ) ) {
		add_action( 'admin_notices', function () {
			?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e( 'WSTB: Please install and activate \'Woomultistore Automated Subsite Billing\'' ); ?></p>
            </div>
			<?php
		} );

		return;
	}

	if ( ! defined( 'GDWPS_ACTIVE' ) ) {
		add_action( 'admin_notices', function () {
			?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e( 'WSTB: Gd Wordpress Support required and should be activated first.' ); ?></p>
            </div>
			<?php
		} );

		return;
	}

	if ( is_main_site() ) {
		return;
	} // this plugin is for subsite only

	GoalDriven\StockTransferBilling\SlicedInvoice\Services\Wstb::init();

	/**
	 * PUC
	 */
	if ( $checker = gd_setup_plugin_update_checker( __FILE__ ) ) {
		//$checker->setBranch( 'master' );
	}
} );