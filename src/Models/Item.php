<?php
/**
 * Created by PhpStorm.
 * User: ihsanberahim
 * Date: 17/05/2019
 * Time: 6:28 AM
 */

namespace GoalDriven\StockTransferBilling\SlicedInvoice\Models;

use Illuminate\Support\Arr;

class Item {
	public $product_SKU;
	public $qty;
	public $product_ID;
	public $title;
	public $date;

	protected $parent_ID;
	protected $meta_id;
	protected $validator;

	static protected $data = [
		'date',
		'product_SKU',
		'qty'
	];

	static protected $rules = [
		'product_SKU' => 'required',
		'qty'         => 'required'
	];

	public function __construct( $product_SKU, $qty = 0, $title = null, $date = null ) {
		$this->validator = gd_validator();

		$this->product_SKU = $product_SKU;
		$this->qty         = $qty;
		$this->date        = $date;

		$this->fill();
	}

	public function data() {
		return Arr::only( (array) $this, self::$data );
	}

	public function fill() {
		$product          = $this->get_product_by_sku( $this->product_SKU );
		$this->product_ID = $product->get_id();
		$this->title      = $product->get_title();

		return $this;
	}

	public function fresh() {
		$query = gd_mysql()->table( 'postmeta' )
		                   ->where( 'post_id', $this->parent_ID )
		                   ->where( 'meta_key', WSTB_MANUAL_STOCK_TRANSFER_META )
		                   ->where( 'meta_value', 'LIKE', "%{$this->product_SKU}%" );

		if ( $result = $query->first() ) {

			$this->meta_id = $result->meta_id;

			$meta_value = unserialize( $result->meta_value );

			foreach ( array_keys( $meta_value ) as $key ) {
				$this->{$key} = data_get( $meta_value, $key, null );
			}
		}

		return $this;
	}

	public function transferred() {
		return gd_carbon_validate( $this->date );
	}

	public function exist() {
		return $this->meta_id && $this->meta_id > 0;
	}

	public function save() {
		$this->delete();

		return add_post_meta( $post_id = $this->parent_ID, WSTB_MANUAL_STOCK_TRANSFER_META, $meta_value = $this->data(), false );
	}

	public function delete() {
		gd_mysql()->table( 'postmeta' )
		          ->where( 'post_id', $this->parent_ID )
		          ->where( 'meta_key', WSTB_MANUAL_STOCK_TRANSFER_META )
		          ->where( 'meta_value', 'LIKE', "%{$this->product_SKU}%" )
		          ->delete();
	}

	/**
	 * HELPERS
	 */
	public function set_parent_ID( $ID ) {
		$this->parent_ID = $ID;

		return $this;
	}

	public static function get_by_item_meta( $item_meta ) {
		$validation = gd_validator()->make(
			$item_meta,
			self::$rules
		);

		if ( $validation->passes() ) {
			return new self( $item_meta['product_SKU'], $item_meta['qty'] );
		}

		wp_die( __( 'product_SKU and qty is required.' ) );
	}

	/**
	 * @param $product_SKU
	 *
	 * @return \WC_Product
	 */
	protected function get_product_by_sku( $product_SKU ) {
		$product_ID = wc_get_product_id_by_sku( $product_SKU );

		if ( $product_ID < 1 ) {
			wp_die( sprintf( __( 'Product SKU (%s) not found in %s.' ), $product_SKU, get_option( 'blogname' ) ) );
		}

		return new \WC_Product( $product_ID );
	}
}