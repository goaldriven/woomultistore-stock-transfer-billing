<?php
/**
 * Created by PhpStorm.
 * User: ihsanberahim
 * Date: 09/05/2019
 * Time: 10:26 PM
 */

namespace GoalDriven\StockTransferBilling\SlicedInvoice\Services;

use GoalDriven\StockTransferBilling\SlicedInvoice\Models\Item;
use GoalDriven\StockTransferBilling\SlicedInvoice\StockTransferMetaBox;
use GoalDriven\StockTransferBilling\SlicedInvoice\StockTransferRest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Optional;

class Wstb {
	protected $ID;
	protected $current_blog_id;

	public function __construct( $ID = null ) {
		global $post;

		if ( ! $ID === null && $post ) {
			$this->ID = $post->ID;
		} else if ( $ID !== null ) {
			$this->ID = $ID;
		}

		$this->current_blog_id = get_current_blog_id();
	}

	public static function init() {
		add_action( WASB_TRACK_STOCK_TRANSFER_ACTION, WSTB_TRACK_STOCK_TRANSFER_ACTION, 10, 2 );

		$metabox = new StockTransferMetaBox();
		$metabox->register_hooks();

		$rest = new StockTransferRest();
		$rest->register_rest_api();
	}


	public function delete_item_by_sku( $product_SKU ) {
		$item = new Item( $product_SKU );
		$item->set_parent_ID( $this->ID );

		if( $item->fresh()->transferred() ) {
			wp_die( __( 'Transfer item cannot be cancelled or updated' ) );
		}

		$item->delete();

		return $this;
	}


	public function add_item( $product_SKU, $qty ) {
		$item = new Item( $product_SKU, $qty );
		$item->set_parent_ID( $this->ID );

		if( $item->fresh()->transferred() ) {
			wp_die( __( 'Transfer item cannot be cancelled or updated' ) );
		}

		$item->save();

		return $this;
	}

	/**
	 * @return \Illuminate\Support\Collection;
	 */
	public function get_items() {
		return collect(
			get_post_meta( $this->ID, WSTB_MANUAL_STOCK_TRANSFER_META, false )
		)->filter( function ( $item_meta ) {
			return $item_meta !== null;
		} )->map( function ( $item_meta ) {
			return Item::get_by_item_meta( $item_meta )->set_parent_ID( $this->ID )->fresh();
		} );
	}

	/**
	 * @param $product_SKU
	 * @param $qty
	 *
	 * @return bool
	 */
	public static function process_transfer( $product_SKU, $qty ) {
		$product_id = wc_get_product_id_by_sku( $product_SKU );
		$operation  = $qty > 0 ? WSTB_WC_INCREASE : WSTB_WC_DECREASE;

		$transfer_subsite = wc_update_product_stock( $product_id, absint( $qty ), $operation );

		switch_to_blog( BLOG_ID_CURRENT_SITE );

		$main_product_id = wc_get_product_id_by_sku( $product_SKU );
		$main_operation  = $qty > 0 ? WSTB_WC_DECREASE : WSTB_WC_INCREASE;

		if ( $transfer_subsite ) {
			$transfer_mainsite = wc_update_product_stock( $main_product_id, absint( $qty ), $main_operation );
		}

		restore_current_blog();

		return $transfer_subsite && $transfer_mainsite;
	}
}