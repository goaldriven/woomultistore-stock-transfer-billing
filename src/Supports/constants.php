<?php defined('ABSPATH') or die(-1);


defined( 'WSTB_TRACK_STOCK_TRANSFER_ACTION' )
	or define( 'WSTB_TRACK_STOCK_TRANSFER_ACTION', 'wstb_track_stock_transfer_action' );

defined( 'WSTB_TRANSFER_TYPE_TRANSFER' )
	or define( 'WSTB_TRANSFER_TYPE_TRANSFER', __( 'Transfer' ) );

defined( 'WSTB_TRANSFER_TYPE_RETURN' )
	or define( 'WSTB_TRANSFER_TYPE_RETURN', __( 'Return' ) );

defined( 'WSTB_STOCK_TRANSFER_META' )
	or define( 'WSTB_STOCK_TRANSFER_META', '_stock_transfer' );

defined( 'WSTB_MANUAL_STOCK_TRANSFER_META' )
	or define( 'WSTB_MANUAL_STOCK_TRANSFER_META', '_manual_stock_transfer' );

defined( 'WSTB_STOCK_TRANSFER_RECORDS_META' )
	or define( 'WSTB_STOCK_TRANSFER_RECORDS_META', '_stock_transfer_records' );

defined( 'WSTB_LAST_STOCK_TRANSFER_TIME_META' )
	or define( 'WSTB_LAST_STOCK_TRANSFER_TIME_META', '_stock_transfer_time' );

defined( 'WSTB_NONCE_NAME' )
	or define( 'WSTB_NONCE_NAME', 'wstb_nonce' );

defined( 'WSTB_NONCE_ACTION' )
	or define( 'WSTB_NONCE_ACTION', 'wstb_nonce_action' );

defined( 'WSTB_REST_NAMESPACE' )
	or define( 'WSTB_REST_NAMESPACE', 'goaldriven/woomultistore-stock-transfer-billing/v1' );

defined( 'WSTB_API_SAVE_STOCK_TRANSFER' )
	or define( 'WSTB_API_SAVE_STOCK_TRANSFER', '/stock-swap' );

defined( 'WSTB_API_ADD_STOCK_TRANSFER_ITEM' )
	or define( 'WSTB_API_ADD_STOCK_TRANSFER_ITEM', WSTB_API_SAVE_STOCK_TRANSFER . '/add-item' );

defined( 'WSTB_API_DELETE_STOCK_TRANSFER_ITEM' )
	or define( 'WSTB_API_DELETE_STOCK_TRANSFER_ITEM', WSTB_API_SAVE_STOCK_TRANSFER . '/delete-item' );

defined( 'WSTB_API_TRANSFER_STOCK' )
	or define( 'WSTB_API_TRANSFER_STOCK', WSTB_API_SAVE_STOCK_TRANSFER . '/transfer' );

defined( 'WSTB_API_REVERSE_TRANSFER' )
	or define( 'WSTB_API_REVERSE_TRANSFER', WSTB_API_SAVE_STOCK_TRANSFER . '/reverse' );

defined( 'WSTB_WC_INCREASE' )
	or define( 'WSTB_WC_INCREASE', 'increase' );

defined( 'WSTB_WC_DECREASE' )
	or define( 'WSTB_WC_DECREASE', 'decrease' );


