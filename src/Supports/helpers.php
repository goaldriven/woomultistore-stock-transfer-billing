<?php defined('ABSPATH') or die(-1);

if( !function_exists( 'wstb_comp' ) ) {
	function wstb_comp() {
		return new \GoalDriven\Supports\Services\Component( __FILE__ );
	}
}