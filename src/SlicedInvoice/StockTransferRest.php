<?php
/**
 * Created by PhpStorm.
 * User: ihsanberahim
 * Date: 09/05/2019
 * Time: 10:47 PM
 */

namespace GoalDriven\StockTransferBilling\SlicedInvoice;


use GoalDriven\StockTransferBilling\SlicedInvoice\Models\Item;
use GoalDriven\StockTransferBilling\SlicedInvoice\Services\Wstb;
use Illuminate\Support\Carbon;

/**
 * Class StockTransferRest
 * @package GoalDriven\StockTransferBilling\SlicedInvoice
 */
class StockTransferRest {
	protected $pluginDir;
	protected $request;
	protected $validator;
	protected $ID;
	protected $wstb;

	public function __construct() {
		$this->pluginDir       = gd_get_plugin_dir( __DIR__ );
		$this->request         = gd_request();
		$this->rest_permission = new StockTransferRestPermission();
		$this->ID              = $this->request->input( 'ID' );
		$this->wstb            = ( new Wstb( $this->ID ) );
	}

	public function add_item() {
		/*
		 * SAMPLE:
		new_item[product_SKU]: A001
		new_item[qty]: 1
		ID: 173
		 */

		// 1) validate sku
		$product_SKU = $this->request->input( 'new_item.product_SKU' );
		$qty         = $this->request->input( 'new_item.qty' );

		return $this->wstb->add_item( $product_SKU, $qty )->get_items();
	}

	public function delete_item() {
		$product_SKU = $this->request->input( 'product_SKU' );

		return $this->wstb->delete_item_by_sku( $product_SKU )->get_items();
	}

	public function transfer() {
		/*
		 * SAMPLE:
		product_SKU: A001
		ID: 173
		 */

		$product_SKU = $this->request->input( 'product_SKU' );
		$item        = ( new Item( $product_SKU ) )->set_parent_ID( $this->ID )->fresh();

		gd_log()->info( json_encode( $item ) );

		gd_mysql()->generalLogOn();

		$prcess_result = Wstb::process_transfer( $product_SKU, $item->qty );

		if ( $prcess_result ) {

			gd_log()->info( json_encode( $item->data() ) );

			$item->date = gd_now_formatted();
			$item->save();

			gd_mysql()->generalLogOff();

		} else {

			gd_mysql()->generalLogOff();

			wp_die( __( 'Transfer unsuccessful' ) );
		}

		return $this->wstb->get_items();
	}

	public function reverse() {
		/*
		 * SAMPLE:
		product_SKU: A001
		ID: 173
		 */

		$product_SKU = $this->request->input( 'product_SKU' );
		$item        = ( new Item( $product_SKU ) )->set_parent_ID( $this->ID )->fresh();

		if ( ! $item->transferred() ) {
			wp_die( sprintf( __( 'Item with product SKU %s not transferred yet.' ), $product_SKU ) );
		}

		$prcess_result = Wstb::process_transfer( $product_SKU, $item->qty * - 1 );

		if ( $prcess_result ) {

			$item->date = null;
			$item->save();

		} else {
			wp_die( __( 'Reverse transfer unsuccessful' ) );
		}

		return $this->wstb->get_items();
	}

	/**
	 * HELPERS
	 */
	public static function nonce_field() {
		wp_nonce_field( WSTB_NONCE_ACTION, WSTB_NONCE_NAME );
	}

	public static function nonce() {
		return wp_create_nonce( WSTB_NONCE_ACTION );
	}

	public function register_rest_api() {
		require_once( $this->pluginDir . '/routes/stock-transfer.php' );
	}
}