<?php
/**
 * Created by PhpStorm.
 * User: ihsanberahim
 * Date: 09/05/2019
 * Time: 10:29 PM
 */

namespace GoalDriven\StockTransferBilling\SlicedInvoice;

defined( 'ABSPATH' ) or die( - 1 );

use GoalDriven\StockTransferBilling\SlicedInvoice\Services\Wstb;
use Illuminate\Support\Carbon;

class StockTransferMetaBox {

	protected $pluginDir;
	protected $request;
	protected $validator;

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->pluginDir = gd_get_plugin_dir( __DIR__ );
		$this->request   = gd_request();
		$this->validator = gd_validator();
	}


	/**
	 * Renders the meta box.
	 */
	public function render_metabox( $post ) {
		$rows = $this->get_stock_transfer_by_post_id( $post->ID );

		//$stock_swap = get_post_meta( $post->ID, WSTB_STOCK_SWAP_META, true );

		$stock_transfer = ( new Wstb( $post->ID ) )->get_items()->toArray();

		include $this->pluginDir . '/resources/views/metabox-stock_transfer.php';
	}


	/**
	 * Handles saving the meta box.
	 *
	 * @param int $post_id Post ID.
	 * @param \WP_Post $post Post object.
	 *
	 * @return null
	 */
	public function save_metabox( $post_id, $post ) {
		// Check if nonce is valid.
		if ( ! $this->nonce_verified() ) {
			return;
		}

		// Check if user has permissions to save data.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		// Check if not an autosave.
		if ( wp_is_post_autosave( $post_id ) ) {
			return;
		}

		// Check if not a revision.
		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

		$current_pricing_group = wasb_get_current_role();
		$current_sliced_item   = get_post_meta( $post_id, '_sliced_items', true );
		$unique_item_reference = 'product_SKU';

		// loop transform stock transfer items to line items
		$wstb = ( new Wstb( $post_id ) );

		$line_items = $wstb->get_items()->map( function ( $item ) use ( $current_pricing_group ) {

			/** @var \GoalDriven\StockTransferBilling\SlicedInvoice\Models\Item $item */
			list( $unit_price, $row_total ) = wasb_line_item_total(
				$item->product_ID,
				$item->qty,
				$current_pricing_group,
				$pricing_key = 'selling_price' );

			$line_item = [
				'qty'         => "{$item->qty}",
				'title'       => sprintf( '%s %s', $item->title, $item->product_SKU ),
				'tax'         => "0",
				'amount'      => $unit_price,
				'description' => $item->transferred()
					? sprintf( __( 'Delivered on %s.' ), $item->date )
					: __( 'Pending.' ),
				'product_ID'  => $item->product_ID,
				'product_SKU' => $item->product_SKU
			];

			return $line_item;
		} );

		// find existing synced stock transfer item, then update
		$touched_line_items = collect( $current_sliced_item )->map( function ( $sliced_item ) use ( $line_items, $unique_item_reference ) {
			$product_ID = data_get( $sliced_item, $unique_item_reference );
			$new_data   = $line_items->where( $unique_item_reference, $product_ID )->first();

			if ( $new_data ) {
				return array_merge( $sliced_item, $new_data );
			}

			return $sliced_item;
		} )->filter( function ( $sliced_item ) {
			// Strip error transfer
			$product_ID = data_get( $sliced_item, 'product_ID', null );

			if ( $product_ID > 0 && empty( data_get( $sliced_item, 'product_SKU', null ) ) ) {
				return false;
			}

			return true;
		} );

		$new_items = $line_items->whereNotIn( $unique_item_reference, $touched_line_items->pluck( $unique_item_reference ) );

		$updated_sliced_item = array_merge( $touched_line_items->toArray(), $new_items->toArray() );

		update_post_meta( $post_id, '_sliced_items', $updated_sliced_item );
	}


	/**
	 * HELPERS
	 */
	public function get_stock_transfer_by_post_id( $post_id ) {
		$rows = get_post_meta( $post_id, WSTB_STOCK_TRANSFER_META, true );

		return $rows
			? collect( $rows )->map( function ( $item ) use ( $post_id ) {
				$item = optional( (object) $item );

				// GET TRANSFER DATE

				$item->transfer_date = get_post_meta( $post_id, WSTB_LAST_STOCK_TRANSFER_TIME_META . $item->product_ID, true );

				if ( ! $item->transfer_date ) {
					$item->transfer_date         = null;
					$item->formattedTransferDate = '-';
				} else {
					$item->formattedTransferDate = $item->transfer_date;
				}

				// DETERMINE TRANSFER STATUS
				$item->transferred = $item->transfer_date && Carbon::parse( $item->transfer_date )->isValid() ? 1 : null;

				switch ( $item->transferred ) {
					case 1:
						$item->formattedTransferred = __( 'Transferred' );
						break;
					default:
						$item->transferred          = null;
						$item->formattedTransferred = __( 'Pending Transfer' );
						break;
				}

				$item->availableTransferType = WSTB_TRANSFER_TYPE_TRANSFER;

				if ( $item->transferred === 1 ) {
					$item->availableTransferType = WSTB_TRANSFER_TYPE_RETURN;
				}

				return $item;
			} )
			: collect( [] );
	}


	public function get_stock_swap_by_post_id( $post_id ) {
		$rows = get_post_meta( $post_id, WSTB_MANUAL_STOCK_TRANSFER_META, true );

		return $rows
			? collect( $rows )->map( function ( $item ) use ( $post_id ) {
				$item = optional( (object) $item );

				// GET TRANSFER DATE

				$item->transfer_date = get_post_meta( $post_id, WSTB_LAST_STOCK_TRANSFER_TIME_META . $item->product_ID, true );

				if ( ! $item->transfer_date ) {
					$item->transfer_date         = null;
					$item->formattedTransferDate = '-';
				} else {
					$item->formattedTransferDate = $item->transfer_date;
				}

				// DETERMINE TRANSFER STATUS
				$item->transferred = $item->transfer_date && Carbon::parse( $item->transfer_date )->isValid() ? 1 : null;

				switch ( $item->transferred ) {
					case 1:
						$item->formattedTransferred = __( 'Transferred' );
						break;
					default:
						$item->transferred          = null;
						$item->formattedTransferred = __( 'Pending Transfer' );
						break;
				}

				$item->availableTransferType = WSTB_TRANSFER_TYPE_TRANSFER;

				if ( $item->transferred === 1 ) {
					$item->availableTransferType = WSTB_TRANSFER_TYPE_RETURN;
				}

				return $item;
			} )
			: collect( [] );
	}


	public function register_hooks() {
		if ( is_admin() ) {
			add_action( 'load-post.php', array( $this, 'init_metabox' ) );
			add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );

			// Add custom line item fields
			add_action( 'sliced_after_line_items', array( $this, 'sliced_after_line_items' ), 2, 999 );
		}
	}


	/**
	 * Meta box initialization.
	 */
	public function init_metabox() {
		add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
		add_action( 'save_post', array( $this, 'save_metabox' ), 9999, 2 );
	}


	/**
	 * Adds the meta box.
	 */
	public function add_metabox() {
		add_meta_box(
			'wstb_stock_transfer',
			__( 'Stock Transfer' ),
			array( $this, 'render_metabox' ),
			'sliced_invoice',
			'advanced',
			'default'
		);

		add_meta_box(
			'wstb_stock_transfer',
			__( 'Stock Transfer' ),
			array( $this, 'render_metabox' ),
			'sliced_quote',
			'advanced',
			'default'
		);
	}

	/**
	 * sliced_after_line_items
	 *
	 * Add custom line item fields
	 *
	 * @param string|false $line_items_group_id
	 * @param \CMB2 $line_items
	 *
	 * @return void
	 */
	public function sliced_after_line_items( $line_items_group_id, $line_items ) {
		$line_items->add_group_field( $line_items_group_id, array(
			'name'       => __( 'Product ID' ),
			'id'         => 'product_ID',
			'type'       => 'hidden',
			'attributes' => array(
				'class' => 'item_product_ID',
			),
		) );

		$line_items->add_group_field( $line_items_group_id, array(
			'name'       => __( 'Product SKU' ),
			'id'         => 'product_SKU',
			'type'       => 'text',
			'attributes' => array(
				'class' => 'item_product_SKU',
				'readonly' => 'readonly'
			),
		) );
	}


	public function endpoint( $uri ) {
		return home_url(
			implode( '', [ 'index.php?rest_route=/', WSTB_REST_NAMESPACE, $uri ] ),
			is_ssl() ? 'https' : 'http'
		);
	}


	public function nonce_verified() {
		// Add nonce for security and authentication.
		$nonce = $this->request->input( WSTB_NONCE_NAME, $this->request->input( 'nonce', '' ) );

		// Check if nonce is valid.
		if ( $nonce !== StockTransferRest::nonce() ) {
			return false;
		}

		return true;
	}
}