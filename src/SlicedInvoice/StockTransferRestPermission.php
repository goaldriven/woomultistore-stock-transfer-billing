<?php
/**
 * Created by PhpStorm.
 * User: ihsanberahim
 * Date: 18/05/2019
 * Time: 1:10 PM
 */

namespace GoalDriven\StockTransferBilling\SlicedInvoice;

/**
 * Class StockTransferRestPermission
 * @package GoalDriven\StockTransferBilling\SlicedInvoice
 * @property-read \Illuminate\Http\Request $request;
 * @property-read \GoalDriven\Supports\ValidatorFactory $validator;
 */
class StockTransferRestPermission {
	protected $request;
	protected $validator;

	public function __construct() {
		$this->request   = gd_request();
		$this->validator = gd_validator();
	}

	public function general() {
		$v = $this->validator->make(
			$this->request->all(),
			[
				'product_SKU' => 'required',
				'ID'          => 'required'
			]
		);

		if ( $v->fails() ) {
			wp_die( __( 'Product SKU and ID is required' ) );
		}

		return true;
	}

	public function add_item() {
		$v = $this->validator->make(
			$this->request->all(),
			[
				'new_item.product_SKU' => 'required',
				'new_item.qty'         => 'required',
				'ID'                   => 'required',
			]
		);

		if ( $v->fails() ) {
			wp_die( __( 'Product SKU, quantity and billing ID are required' ) );
		}

		return true;
	}
}
