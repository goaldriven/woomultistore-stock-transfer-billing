=== Woomultistore Stock Transfer Billing ===
Tags: woocommerce, woomultistore
Requires at least: 4.5
Tested up to: 5.1.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
