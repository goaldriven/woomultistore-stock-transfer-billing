<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'GoalDriven\\StockTransferBilling\\SlicedInvoice\\Models\\Item' => $baseDir . '/src/Models/Item.php',
    'GoalDriven\\StockTransferBilling\\SlicedInvoice\\Services\\Wstb' => $baseDir . '/src/Services/Wstb.php',
    'GoalDriven\\StockTransferBilling\\SlicedInvoice\\SlicedInvoiceItem' => $baseDir . '/src/SlicedInvoice/SlicedInvoiceItem.php',
    'GoalDriven\\StockTransferBilling\\SlicedInvoice\\SlicedInvoiceService' => $baseDir . '/src/SlicedInvoice/SlicedInvoiceService.php',
    'GoalDriven\\StockTransferBilling\\SlicedInvoice\\StockTransferMetaBox' => $baseDir . '/src/SlicedInvoice/StockTransferMetaBox.php',
    'GoalDriven\\StockTransferBilling\\SlicedInvoice\\StockTransferRest' => $baseDir . '/src/SlicedInvoice/StockTransferRest.php',
    'GoalDriven\\StockTransferBilling\\SlicedInvoice\\StockTransferRestPermission' => $baseDir . '/src/SlicedInvoice/StockTransferRestPermission.php',
);
