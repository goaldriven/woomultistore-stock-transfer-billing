<?php
/**
 * Created by PhpStorm.
 * User: ihsanberahim
 * Date: 02/06/2019
 * Time: 10:14 AM
 */

defined( 'ABSPATH' ) or die( '' );


add_action( 'rest_api_init', function () {

	register_rest_route( WSTB_REST_NAMESPACE, WSTB_API_ADD_STOCK_TRANSFER_ITEM, array(
		'methods'             => \WP_REST_Server::CREATABLE,
		'callback'            => array( $this, 'add_item' ),
		'permission_callback' => array( $this->rest_permission, 'add_item' )
	) );

	register_rest_route( WSTB_REST_NAMESPACE, WSTB_API_DELETE_STOCK_TRANSFER_ITEM, array(
		'methods'             => \WP_REST_Server::DELETABLE,
		'callback'            => array( $this, 'delete_item' ),
		'permission_callback' => array( $this->rest_permission, 'general' )
	) );

	register_rest_route( WSTB_REST_NAMESPACE, WSTB_API_TRANSFER_STOCK, array(
		'methods'             => \WP_REST_Server::CREATABLE,
		'callback'            => array( $this, 'transfer' ),
		'permission_callback' => array( $this->rest_permission, 'general' )
	) );

	register_rest_route( WSTB_REST_NAMESPACE, WSTB_API_REVERSE_TRANSFER, array(
		'methods'             => \WP_REST_Server::CREATABLE,
		'callback'            => array( $this, 'reverse' ),
		'permission_callback' => array( $this->rest_permission, 'general' )
	) );
} );