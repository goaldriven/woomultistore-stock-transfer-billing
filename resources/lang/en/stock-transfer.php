<?php
/**
 * Created by PhpStorm.
 * User: ihsanberahim
 * Date: 02/06/2019
 * Time: 10:44 AM
 */

defined('ABSPATH') or die('');

return [
	'Item' => __('Item'),
	'Qty' => __('Qty'),
	'Date' => __('Date'),
	'Status' => __('Status'),
	'X' => __('X'),
	'Transfer' => __('Transfer'),
	'Undo' => __('Undo'),
	'Delivered' => __('Delivered'),
	'Pending' => __('Pending'),
	'New transfer added.' => __('New transfer added.'),
	'New transfer not added.' => __('New transfer not added.'),
	'Transfer cancelled.' => __('Transfer cancelled.'),
	'Transfer cancellation fail.' => __('Transfer cancellation fail.'),
	'Transfer succeed.' => __('Transfer succeed.'),
	'Transfer fail. Try again' => __('Transfer fail. Try again'),
	'Undo successful.' => __('Undo successful.'),
	'Undo unsuccessful. Try again' => __('Undo unsuccessful. Try again')
];