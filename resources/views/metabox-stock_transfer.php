<?php defined( 'ABSPATH' ) or die( - 1 );

/**
 * @var \GoalDriven\StockTransferBilling\SlicedInvoice\StockTransferMetaBox $this
 * @var array $rows
 *
 * ref: GoalDriven\StockTransferBilling\SlicedInvoice\StockTransferMetaBox::render_metabox
 * @var array $stock_transfer
 */

global $post;

wp_nonce_field(WSTB_NONCE_ACTION, WSTB_NONCE_NAME);
?>
<!--BEGIN: metabox-sliced_invoice.tpl.php-->
<div id="stock_transfer"></div>

<script>
	<?php
	echo wstb_comp()->requirecss( 'bootstrap-grid', '/assets/plugins/bootstrap/bootstrap.css' );
	echo wstb_comp()->requirecss( 'stock-transfer', '/assets/components/stock-transfer/css/metabox.css' );

	echo wstb_comp()->wp_require_object( 'metaboxStockTransfer', [
		'stock_transfer'           => ! empty( $stock_transfer ) ? $stock_transfer : [],
		'api_transfer_stock'       => esc_url_raw( $this->endpoint( WSTB_API_TRANSFER_STOCK ) ),
		'api_reverse_transfer'     => esc_url_raw( $this->endpoint( WSTB_API_REVERSE_TRANSFER ) ),
		'api_add_transfer_item'    => esc_url_raw( $this->endpoint( WSTB_API_ADD_STOCK_TRANSFER_ITEM ) ),
		'api_delete_transfer_item' => esc_url_raw( $this->endpoint( WSTB_API_DELETE_STOCK_TRANSFER_ITEM ) ),
		'ID'                       => $post ? $post->ID : null,
		'nonce'                    => wp_create_nonce( 'wp_rest' )
	] );

	echo wstb_comp()->wp_require_object( 'metaboxStockTransferi18n', [
		'locale'         => 'en',
		'fallbackLocale' => 'en',
		'messages'       => [
			'en' => include( __DIR__ . '/../lang/en/stock-transfer.php' )
		]
	] );

	echo wstb_comp()->requirejs( 'Vue', '/node_modules/vue/dist/vue.min.js',
		wstb_comp()->requirejs( 'Toasted', '/node_modules/vue-toasted/dist/vue-toasted.min.js',
			wstb_comp()->requirejs( 'VueI18n', '/node_modules/vue-i18n/dist/vue-i18n.min.js',
				wstb_comp()->requirejs( 'objectToFormData', '/assets/plugins/object-to-formdata/index.js',
					wstb_comp()->requirejs( 'axios', '/node_modules/axios/dist/axios.min.js',
						wstb_comp()->requirejs( false, '/assets/components/stock-transfer/js/metabox.js' )
					)
				)
			)
		)
	);

	?>
</script>
<!--END: metabox-sliced_invoice.tpl.php-->