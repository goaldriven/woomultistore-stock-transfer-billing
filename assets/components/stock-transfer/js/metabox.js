jQuery(document).ready(($) => {
    Vue.use(VueI18n);
    Vue.use(Toasted, {
        position: 'bottom-center',
        duration: 3000,
        closeOnSwipe: true,
        singleton: true
    });

    const i18n = new VueI18n(
        metaboxStockTransferi18n ? metaboxStockTransferi18n() : {}
    );

    new Vue({
        el: '#stock_transfer',
        template: `
        <div class="stock-transfer">
            <table class="widefat">
                <thead>
                    <tr>
                        <th>{{ $t( 'Item' ) }}</th>
                        <th class="text-center">{{ $t( 'Qty' ) }}</th>
                        <th class="text-center">{{ $t( 'Date' ) }}</th>
                        <th>{{ $t( 'Status' ) }}</th>
                    </tr>
                </thead>
                <tbody>
                    <!--EXISTING TRANSFER-->
                    <tr v-for="(item, key) in stock_transfer">
                        <td>
                            <label for="item_product_title">Product Name: <span id="item_product_title">{{ item.title }}</span>
                            </label>
                            <br/>
                            <label for="item_product_SKU">SKU: <span id="item_product_SKU">{{ item.product_SKU }}</span>
                            </label>
                        </td>
                        <td class="text-center">{{ item.qty }}</td>
                        <td class="text-center">{{ item.date }}</td>
                        <td>{{ itemStatus( item ) }}
                            <div class="d-flex flex-row justify-content-end">
                                <button v-if="!item.date && item.qty > -1" class="button button-default flex-grow-1 d-block mr-2" @click.prevent="transfer( item, key )">{{ $t('Transfer') }}
                                </button>
                                <button v-if="!item.date && item.qty < 0" class="button button-default flex-grow-1 d-block mr-2" @click.prevent="transfer( item, key )">{{ $t('Transfer') }}
                                </button>
                                <button v-if="!item.date" class="button button-default d-block" @click.prevent="deleteTransfer( item, key )">{{ $t('X') }}
                                </button>
                                <button v-if="item.date" class="button button-default d-block" @click.prevent="undoTransfer( item, key )">{{ $t('Undo') }}
                                </button>
                            </div>
                        </td>
                    </tr>
                    
                    <!--ADD ITEM-->
                    <tr v-if="new_item">
                        <td>
                            <label for="new_item_product_SKU">SKU:<br/>
                                <input type="text" id="new_item_product_SKU" v-model="new_item.product_SKU" placeholder="Product SKU">
                            </label>
                        </td>
                        <td>
                            <label for="new_item_product_qty">QTY:<br/>
                                <input type="number" id="new_item_product_qty" v-model="new_item.qty" placeholder="1">
                            </label>
                        </td>
                        <td></td>
                        <td class="align-bottom">
                            <button class="button button-default" @click.prevent="addTransfer( new_item )">Confirm</button>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="3">
                        </th>
                        <th>
                            <div class="d-flex flex-row justify-content-end">
                                <button class="button button-default mr-2" @click.prevent="addTransfer()">Add Item</button>
                            </div>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
        `,
        i18n,
        data() {
            return Object.assign(metaboxStockTransfer
                    ? metaboxStockTransfer()
                    : {
                        stock_transfer: null,
                        api_transfer_stock: null,
                        api_reverse_transfer: null,
                        api_add_transfer_item: null,
                        api_delete_transfer_item: null,
                        ID: null,
                        nonce: null
                    },
                    {
                        new_item: null
                    });
        },
        mounted() {
        },
        methods: {
            /**
             * ACTIONS
             */
            transfer(item, key) {
                axios.post(unescape(this.api_transfer_stock), this.toFormData({ product_SKU: item.product_SKU }), this.getHeaders())
                    .then((res) => {
                        this.reloadFromResponse(res);

                        Vue.toasted.success(
                            this.$t('Transfer succeed.')
                        );
                    })
                    .catch(() => {
                        Vue.toasted.error(
                            this.$t('Transfer fail. Try again')
                        );
                    })
            },
            undoTransfer( item, key ) {
                axios.post(unescape(this.api_reverse_transfer), this.toFormData({ product_SKU: item.product_SKU }), this.getHeaders())
                    .then((res) => {
                        this.reloadFromResponse(res);

                        Vue.toasted.success(
                            this.$t('Undo successful.')
                        );
                    })
                    .catch(() => {
                        Vue.toasted.error(
                            this.$t('Undo unsuccessful. Try again')
                        );
                    })
            },
            addTransfer( new_item ) {
                // Start new item
                if( !new_item ) {
                    this.new_item = {
                        product_SKU: null,
                        qty: null
                    };

                    return false;
                }

                // trim product_SKU
                this.new_item.product_SKU = this.new_item.product_SKU.trim();

                axios.post(unescape(this.api_add_transfer_item), this.toFormData({ new_item }),
                    this.getHeaders())
                    .then((res) => {
                        this.reloadFromResponse(res);

                        this.new_item = null;

                        Vue.toasted.success(
                            this.$t('New transfer added.')
                        );
                    })
                    .catch((err) => {
                        Vue.toasted.error(
                            this.$t('New transfer not added.') + '&nbsp;' + err.response.data.message
                        );
                    });
            },
            deleteTransfer( item, key ) {
                axios.delete(unescape(this.api_delete_transfer_item), {
                        params: {
                            ID: this.ID,
                            product_SKU: item.product_SKU
                        }
                    },
                    this.getHeaders())
                    .then((res) => {
                        this.reloadFromResponse(res);

                        Vue.toasted.success(
                            this.$t('Transfer cancelled.')
                        );
                    })
                    .catch((err) => {
                        Vue.toasted.error(
                            this.$t('Transfer cancellation fail.') + '&nbsp;' + err.response.data.message
                        );
                    });
            },

            /**
             * HELPERS
             */
            toFormData( input ) {
                input.ID = this.ID;

                return objectToFormData(
                    input,
                    {
                        indices: false,
                        nulls: true
                    },
                    new FormData());
            },
            reloadFromResponse( res ) {
                Vue.set(this, 'stock_transfer', []);

                _.chain(res.data).each((item) => {
                    this.stock_transfer.push( item );
                });
            },
            getHeaders() {
                return {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'X-WP-Nonce': this.nonce
                    }
                };
            },
            itemStatus(item) {
                return item && item.date && item.date.length > 6
                    ? this.$t('Delivered')
                    : this.$t('Pending');
            }
        }
    });
});

jQuery(document).ready(($) => {
    // Remove empty item if item_qty and item_amount are empty
    $('#publish').on('click', function() {
        const cmbRow = $('.postbox.cmb-row.cmb-repeatable-grouping');

        cmbRow.each(function() {
            const row = $(this);

            if( _.isEmpty(row.find('.item_qty').val())
                || _.isEmpty(row.find('.item_amount').val()) ) {
                row.remove();
            }
        });

    });
});